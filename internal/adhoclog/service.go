package adhoclog

import (
	"context"

	"gitlab.com/itbis/loena-api/internal/entity"
	"gitlab.com/itbis/loena-api/pkg/log"
)

// Service encapsulates usecase logic for adhoclogs.
type Service interface {
	Get(ctx context.Context, id string) ([]Adhoclog, error)
	Query(ctx context.Context, offset, limit int) ([]Adhoclog, error)
	Count(ctx context.Context) (int, error)
}

// Adhoclog represents the data about an adhoclog.
type Adhoclog struct {
	entity.Adhoclog
}

type service struct {
	repo   Repository
	logger log.Logger
}

// NewService creates a new adhoclog service.
func NewService(repo Repository, logger log.Logger) Service {
	return service{repo, logger}
}

// Get returns the adhoclog with the specified the adhoclog ID.
func (s service) Get(ctx context.Context, id string) ([]Adhoclog, error) {
	items, err := s.repo.Get(ctx, id)
	if err != nil {
		return nil, err
	}
	result := []Adhoclog{}
	for _, item := range items {
		result = append(result, Adhoclog{item})
	}
	return result, nil
}

// Count returns the number of adhoclogs.
func (s service) Count(ctx context.Context) (int, error) {
	return s.repo.Count(ctx)
}

// Query returns the adhoclogs with the specified offset and limit.
func (s service) Query(ctx context.Context, offset, limit int) ([]Adhoclog, error) {
	items, err := s.repo.Query(ctx, offset, limit)
	if err != nil {
		return nil, err
	}
	result := []Adhoclog{}
	for _, item := range items {
		result = append(result, Adhoclog{item})
	}
	return result, nil
}
