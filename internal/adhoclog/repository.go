package adhoclog

import (
	"context"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"gitlab.com/itbis/loena-api/internal/entity"
	"gitlab.com/itbis/loena-api/pkg/dbcontext"
	"gitlab.com/itbis/loena-api/pkg/log"
)

// Repository encapsulates the logic to access adhoclogs from the data source.
type Repository interface {
	// Get returns the adhoclog with the specified adhoclog ID.
	Get(ctx context.Context, id string) ([]entity.Adhoclog, error)

	// Count returns the number of adhoclogs.
	Count(ctx context.Context) (int, error)
	// Query returns the list of adhoclogs with the given offset and limit.
	Query(ctx context.Context, offset, limit int) ([]entity.Adhoclog, error)
}

// repository persists adhoclogs in database
type repository struct {
	db     *dbcontext.DB
	logger log.Logger
}

// NewRepository creates a new adhoclog repository
func NewRepository(db *dbcontext.DB, logger log.Logger) Repository {
	return repository{db, logger}
}

// Get reads the adhoclog with the specified ID from the database.
func (r repository) Get(ctx context.Context, id string) ([]entity.Adhoclog, error) {
	var adhoclog []entity.Adhoclog
	err := r.db.With(ctx).Select().Where(dbx.HashExp{"msisdn": id}).All(&adhoclog)
	return adhoclog, err
}

// Count returns the number of the adhoclog records in the database.
func (r repository) Count(ctx context.Context) (int, error) {
	var count int
	table := entity.Adhoclog{}
	err := r.db.With(ctx).Select("COUNT(*)").From(table.TableName()).Row(&count)
	return count, err
}

// Query retrieves the adhoclog records with the specified offset and limit from the database.
func (r repository) Query(ctx context.Context, offset, limit int) ([]entity.Adhoclog, error) {
	var adhoclogs []entity.Adhoclog
	err := r.db.With(ctx).
		Select().
		OrderBy("id").
		Offset(int64(offset)).
		Limit(int64(limit)).
		All(&adhoclogs)
	return adhoclogs, err
}
