package entity

// User represents a BidLoan.
type Bidloan struct {
	LoanBusinessID    string `db:"pk" json:"loan_business_id"`
	LoanOfferID       string `json:"loan_offer_id"`
	LoanPrice         int    `json:"loan_price"`
	PaymentBusinessID string `json:"payment_business_id"`
	PaymentOfferID    string `json:"payment_offer_id"`
}

// get table real
func (c Bidloan) TableName() string {
	return "loan_payment_mapping"
}
