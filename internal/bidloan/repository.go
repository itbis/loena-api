package bidloan

import (
	"context"

	dbx "github.com/go-ozzo/ozzo-dbx"
	"gitlab.com/itbis/loena-api/internal/entity"
	"gitlab.com/itbis/loena-api/pkg/dbcontext"
	"gitlab.com/itbis/loena-api/pkg/log"
)

// Repository encapsulates the logic to access bidloans from the data source.
type Repository interface {
	// Get returns the bidloan with the specified bidloan ID.
	Get(ctx context.Context, id string) (entity.Bidloan, error)
	// Count returns the number of bidloans.
	Count(ctx context.Context) (int, error)
	// Query returns the list of bidloans with the given offset and limit.
	QueryByPrice(ctx context.Context, id string) ([]entity.Bidloan, error)
	// Query returns the list of bidloans with the given offset and limit.
	Query(ctx context.Context, offset, limit int) ([]entity.Bidloan, error)

	// Create saves a new bidloan in the storage.
	Create(ctx context.Context, bidloan entity.Bidloan) error
	// Update updates the bidloan with given ID in the storage.
	Update(ctx context.Context, bidloan entity.Bidloan) error
	// Delete removes the bidloan with given ID from the storage.
	Delete(ctx context.Context, id string) error
}

// repository persists bidloans in database
type repository struct {
	db     *dbcontext.DB
	logger log.Logger
}

// NewRepository creates a new bidloan repository
func NewRepository(db *dbcontext.DB, logger log.Logger) Repository {
	return repository{db, logger}
}

// Get reads the bidloan with the specified ID from the database.
func (r repository) Get(ctx context.Context, id string) (entity.Bidloan, error) {
	var bidloan entity.Bidloan
	err := r.db.With(ctx).Select().Model(id, &bidloan)
	return bidloan, err
}

// Create saves a new bidloan record in the database.
// It returns the ID of the newly inserted bidloan record.
func (r repository) Create(ctx context.Context, bidloan entity.Bidloan) error {

	return r.db.With(ctx).Model(&bidloan).Insert()
}

// Update saves the changes to an bidloan in the database.
func (r repository) Update(ctx context.Context, bidloan entity.Bidloan) error {
	return r.db.With(ctx).Model(&bidloan).Update()

}

// Delete deletes an bidloan with the specified ID from the database.
func (r repository) Delete(ctx context.Context, id string) error {
	bidloan, err := r.Get(ctx, id)
	if err != nil {
		return err
	}
	return r.db.With(ctx).Model(&bidloan).Delete()
}

// Count returns the number of the bidloan records in the database.
func (r repository) Count(ctx context.Context) (int, error) {
	var count int
	table := entity.Bidloan{}
	err := r.db.With(ctx).Select("COUNT(*)").From(table.TableName()).Row(&count)
	return count, err
}

// Query retrieves the bidloan records with the specified offset and limit from the database.
func (r repository) Query(ctx context.Context, offset, limit int) ([]entity.Bidloan, error) {
	var bidloans []entity.Bidloan
	err := r.db.With(ctx).
		Select().
		OrderBy("loan_business_id").
		Offset(int64(offset)).
		Limit(int64(limit)).
		All(&bidloans)
	return bidloans, err
}

// Query retrieves the bidloan records with the specified offset and limit from the database.
func (r repository) QueryByPrice(ctx context.Context, id string) ([]entity.Bidloan, error) {
	var bidloans []entity.Bidloan
	err := r.db.With(ctx).
		Select().
		Where(dbx.HashExp{"loan_price": id}).
		OrderBy("loan_business_id").
		All(&bidloans)
	return bidloans, err
}
