package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	dbx "github.com/go-ozzo/ozzo-dbx"
	routing "github.com/go-ozzo/ozzo-routing/v2"
	"github.com/go-ozzo/ozzo-routing/v2/content"
	"github.com/go-ozzo/ozzo-routing/v2/cors"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	"gitlab.com/itbis/loena-api/internal/adhoc"
	"gitlab.com/itbis/loena-api/internal/adhoclog"
	"gitlab.com/itbis/loena-api/internal/auth"
	"gitlab.com/itbis/loena-api/internal/bidloan"
	"gitlab.com/itbis/loena-api/internal/config"
	"gitlab.com/itbis/loena-api/internal/customer"
	"gitlab.com/itbis/loena-api/internal/errors"
	"gitlab.com/itbis/loena-api/internal/healthcheck"
	"gitlab.com/itbis/loena-api/internal/history"
	"gitlab.com/itbis/loena-api/internal/menu"
	"gitlab.com/itbis/loena-api/internal/reminder"
	"gitlab.com/itbis/loena-api/internal/user"
	"gitlab.com/itbis/loena-api/pkg/accesslog"
	"gitlab.com/itbis/loena-api/pkg/dbcontext"
	"gitlab.com/itbis/loena-api/pkg/log"
)

// Version indicates the current version of the application.
var Version = "1.0.0"
var flagConfig = flag.String("config", "./config/local.yml", "path to the config file")

func main() {
	flag.Parse()
	// create root logger tagged with server version
	logger := log.New().With(nil, "version", Version)

	// load application configurations
	cfg, err := config.Load(*flagConfig, logger)
	if err != nil {
		logger.Errorf("failed to load application configuration: %s", err)
		os.Exit(-1)
	}

	// connect to the database
	db, err := dbx.MustOpen("mysql", cfg.DSN)
	if err != nil {
		logger.Error(err)
		os.Exit(-1)
	}
	db.QueryLogFunc = logDBQuery(logger)
	db.ExecLogFunc = logDBExec(logger)
	defer func() {
		if err := db.Close(); err != nil {
			logger.Error(err)
		}
	}()

	// connect to the database customer
	dbCustomer, err := dbx.MustOpen("mysql", cfg.DSNCustomer)
	if err != nil {
		logger.Error(err)
		os.Exit(-1)
	}
	dbCustomer.QueryLogFunc = logDBQuery(logger)
	dbCustomer.ExecLogFunc = logDBExec(logger)
	defer func() {
		if err := dbCustomer.Close(); err != nil {
			logger.Error(err)
		}
	}()
	// connect to the database queue
	dbQueue, err := dbx.MustOpen("mysql", cfg.DSNQueue)
	if err != nil {
		logger.Error(err)
		os.Exit(-1)
	}
	dbQueue.QueryLogFunc = logDBQuery(logger)
	dbQueue.ExecLogFunc = logDBExec(logger)
	defer func() {
		if err := dbQueue.Close(); err != nil {
			logger.Error(err)
		}
	}()

	// build HTTP server
	address := fmt.Sprintf(":%v", cfg.ServerPort)
	hs := &http.Server{
		Addr:    address,
		Handler: buildHandler(logger, dbcontext.New(db), dbcontext.New(dbCustomer), dbcontext.New(dbQueue), cfg),
	}

	// start the HTTP server with graceful shutdown
	go routing.GracefulShutdown(hs, 10*time.Second, logger.Infof)
	logger.Infof("server %v is running at %v", Version, address)
	if err := hs.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		logger.Error(err)
		os.Exit(-1)
	}
}

// buildHandler sets up the HTTP routing and builds an HTTP handler.
func buildHandler(logger log.Logger, db, dbCustomer, dbQueue *dbcontext.DB, cfg *config.Config) http.Handler {
	router := routing.New()
	router.Use(
		accesslog.Handler(logger),
		errors.Handler(logger),
		content.TypeNegotiator(content.JSON),
		cors.Handler(cors.AllowAll),
	)

	healthcheck.RegisterHandlers(router, Version)

	rg := router.Group("/loena/v1")

	authHandler := auth.Handler(cfg.JWTSigningKey)
	auth.RegisterHandlers(rg,
		auth.NewService(auth.NewRepository(db, logger), cfg.JWTSigningKey, cfg.JWTExpiration, logger),
		logger,
	)

	user.RegisterHandlers(rg,
		user.NewService(user.NewRepository(db, logger), logger),
		authHandler, logger,
	)
	menu.RegisterHandlers(rg,
		menu.NewService(menu.NewRepository(db, logger), logger),
		authHandler, logger,
	)
	customer.RegisterHandlers(rg,
		customer.NewService(customer.NewRepository(dbCustomer, logger), logger),
		authHandler, logger,
	)
	history.RegisterHandlers(rg,
		history.NewService(history.NewRepository(dbCustomer, logger), logger),
		authHandler, logger,
	)
	reminder.RegisterHandlers(rg,
		reminder.NewService(reminder.NewRepository(dbQueue, logger), logger),
		authHandler, logger,
	)
	adhoc.RegisterHandlers(rg,
		adhoc.NewService(adhoc.NewRepository(db, logger), logger),
		authHandler, logger,
	)
	bidloan.RegisterHandlers(rg,
		bidloan.NewService(bidloan.NewRepository(dbCustomer, logger), logger),
		authHandler, logger,
	)
	adhoclog.RegisterHandlers(rg,
		adhoclog.NewService(adhoclog.NewRepository(dbCustomer, logger), logger),
		authHandler, logger,
	)

	return router
}

// logDBQuery returns a logging function that can be used to log SQL queries.
func logDBQuery(logger log.Logger) dbx.QueryLogFunc {
	return func(ctx context.Context, t time.Duration, sql string, rows *sql.Rows, err error) {
		if err == nil {
			logger.With(ctx, "duration", t.Milliseconds(), "sql", sql).Info("DB query successful")
		} else {
			logger.With(ctx, "sql", sql).Errorf("DB query error: %v", err)
		}
	}
}

// logDBExec returns a logging function that can be used to log SQL executions.
func logDBExec(logger log.Logger) dbx.ExecLogFunc {
	return func(ctx context.Context, t time.Duration, sql string, result sql.Result, err error) {
		if err == nil {
			logger.With(ctx, "duration", t.Milliseconds(), "sql", sql).Info("DB execution successful")
		} else {
			logger.With(ctx, "sql", sql).Errorf("DB execution error: %v", err)
		}
	}
}
