-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 15, 2021 at 11:17 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `queue_reminder_payment`
--

-- --------------------------------------------------------

--
-- Table structure for table `queue0`
--

CREATE TABLE `queue0` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loan_value` int(11) NOT NULL,
  `due_date` datetime NOT NULL,
  `incremental` int(11) NOT NULL,
  `transaction_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `queue0`
--

INSERT INTO `queue0` (`id`, `msisdn`, `loan_value`, `due_date`, `incremental`, `transaction_id`) VALUES
(1, '6282124913380', 3000, '2021-01-14 13:09:40', 1, 'r6_628212491338020210112adhocd');

-- --------------------------------------------------------

--
-- Table structure for table `queue1`
--

CREATE TABLE `queue1` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loan_value` int(11) NOT NULL,
  `due_date` datetime NOT NULL,
  `incremental` int(11) NOT NULL,
  `transaction_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `queue2`
--

CREATE TABLE `queue2` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loan_value` int(11) NOT NULL,
  `due_date` datetime NOT NULL,
  `incremental` int(11) NOT NULL,
  `transaction_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `queue3`
--

CREATE TABLE `queue3` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loan_value` int(11) NOT NULL,
  `due_date` datetime NOT NULL,
  `incremental` int(11) NOT NULL,
  `transaction_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `queue4`
--

CREATE TABLE `queue4` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loan_value` int(11) NOT NULL,
  `due_date` datetime NOT NULL,
  `incremental` int(11) NOT NULL,
  `transaction_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `queue5`
--

CREATE TABLE `queue5` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loan_value` int(11) NOT NULL,
  `due_date` datetime NOT NULL,
  `incremental` int(11) NOT NULL,
  `transaction_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `queue6`
--

CREATE TABLE `queue6` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loan_value` int(11) NOT NULL,
  `due_date` datetime NOT NULL,
  `incremental` int(11) NOT NULL,
  `transaction_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `queue6`
--

INSERT INTO `queue6` (`id`, `msisdn`, `loan_value`, `due_date`, `incremental`, `transaction_id`) VALUES
(51, '6282114307936', 10000, '2021-02-26 00:00:00', 8, 'r6175da3483ca07936000274681bb5eSac10000S000');

-- --------------------------------------------------------

--
-- Table structure for table `queue7`
--

CREATE TABLE `queue7` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loan_value` int(11) NOT NULL,
  `due_date` datetime NOT NULL,
  `incremental` int(11) NOT NULL,
  `transaction_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `queue8`
--

CREATE TABLE `queue8` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loan_value` int(11) NOT NULL,
  `due_date` datetime NOT NULL,
  `incremental` int(11) NOT NULL,
  `transaction_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `queue9`
--

CREATE TABLE `queue9` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loan_value` int(11) NOT NULL,
  `due_date` datetime NOT NULL,
  `incremental` int(11) NOT NULL,
  `transaction_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `queue0`
--
ALTER TABLE `queue0`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- Indexes for table `queue1`
--
ALTER TABLE `queue1`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- Indexes for table `queue2`
--
ALTER TABLE `queue2`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- Indexes for table `queue3`
--
ALTER TABLE `queue3`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- Indexes for table `queue4`
--
ALTER TABLE `queue4`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- Indexes for table `queue5`
--
ALTER TABLE `queue5`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- Indexes for table `queue6`
--
ALTER TABLE `queue6`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- Indexes for table `queue7`
--
ALTER TABLE `queue7`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- Indexes for table `queue8`
--
ALTER TABLE `queue8`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- Indexes for table `queue9`
--
ALTER TABLE `queue9`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `msisdn` (`msisdn`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `queue0`
--
ALTER TABLE `queue0`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `queue1`
--
ALTER TABLE `queue1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `queue2`
--
ALTER TABLE `queue2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `queue3`
--
ALTER TABLE `queue3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `queue4`
--
ALTER TABLE `queue4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `queue5`
--
ALTER TABLE `queue5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `queue6`
--
ALTER TABLE `queue6`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `queue7`
--
ALTER TABLE `queue7`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `queue8`
--
ALTER TABLE `queue8`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `queue9`
--
ALTER TABLE `queue9`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
